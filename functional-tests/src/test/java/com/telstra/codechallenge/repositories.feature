# See
# https://github.com/intuit/karate#syntax-guide
# for how to write feature scenarios
Feature: As an api user I want to retrieve repositories on various criteria
    
Scenario: Is the repositories url present and returns data
    Given url microserviceUrl
    And path '/repositories'
    * def query = { noOfRepo: '2' }
    And params query
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    # see https://github.com/intuit/karate#schema-validation
    # Define the required schema
    * def repositorySchema = { "html_url": #string, "name": #string, "description": #string, "language": #string, "watchers_count": #number }   
    # The response should have an array of 2 repository objects
    And match response == '#[2] repositorySchema' 

Scenario: Is the repositories url giving error when requested without param
    Given url microserviceUrl
    And path '/repositories'
    When method GET
    Then status 400