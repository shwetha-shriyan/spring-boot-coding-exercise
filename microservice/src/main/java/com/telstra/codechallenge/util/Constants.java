package com.telstra.codechallenge.util;

public class Constants {

	private Constants() {

	}

	public static final String API_PARAM_SORT = "sort";
	public static final String API_PARAM_ORDER = "order";
	public static final String API_PARAM_QUALIFIER = "q";
	public static final String API_PARAM_PAGE = "page";
	public static final String API_PARAM_PER_PAGE = "per_page";
	public static final String INVALID_INPUT = "Input should be greater than 0";

}
