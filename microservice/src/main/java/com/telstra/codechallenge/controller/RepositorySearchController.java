package com.telstra.codechallenge.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.telstra.codechallenge.exception.InvalidInputException;
import com.telstra.codechallenge.model.Items;
import com.telstra.codechallenge.service.RepositorySearchService;
import com.telstra.codechallenge.util.Constants;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/")
public class RepositorySearchController {

	@Autowired
	RepositorySearchService repositorySearchService;

	/**
	 * 
	 * @param q     is query that contains one or more search keywords and
	 *              qualifiers. Eg: q=topic:tetris
	 * @param sort  represents the value on which we want the list to be sorted. Eg:
	 *              sort=stars
	 * @param order represents the ascending or descending order. Eg:order=desc
	 * @param limit represents the number of list in array that has to be returned
	 * @return array of Items that consists of html_url, name, language, description
	 *         and watchers_count
	 */
	@GetMapping("repositories")
	public ResponseEntity<List<Items>> getRepositories(@RequestParam int noOfRepo) {
		if (noOfRepo <= 0) {
			throw new InvalidInputException(Constants.INVALID_INPUT);
		} else {
			log.info("Get request received: /repositories?noOfRepo={}", noOfRepo);
			List<Items> items = repositorySearchService.getRepositories(noOfRepo);
			return ResponseEntity.status(HttpStatus.OK).body(items);
		}
	}

}
