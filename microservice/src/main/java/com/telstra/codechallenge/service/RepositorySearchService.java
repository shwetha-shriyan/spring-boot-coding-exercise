package com.telstra.codechallenge.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.telstra.codechallenge.model.Items;
import com.telstra.codechallenge.model.Repositories;
import com.telstra.codechallenge.util.Constants;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RepositorySearchService {

	@Value("${api.github.base.url}")
	private String repostoriesSearchBaseUrl;

	@Value("${api.github.param.qualifier.value}")
	private String created;

	@Value("${api.github.param.sort.value}")
	private String sort;

	@Value("${api.github.param.order.value}")
	private String order;

	@Value("${page.per.size}")
	private int perPage;

	@Autowired
	private RestTemplate restTemplate;

	/**
	 * 
	 * @param q     is query that contains one or more search keywords and
	 *              qualifiers. Eg: q=topic:tetris
	 * @param sort  represents the value on which we want the list to be sorted. Eg:
	 *              sort=stars
	 * @param order represents the ascending or descending order. Eg:order=desc
	 * @param limit represents the number of list in array that has to be returned
	 * @return array of Items that consists of html_url, name, language, description
	 *         and watchers_count
	 */
	public List<Items> getRepositories(int noOfRepo) {

		Calendar currentCalendar = Calendar.getInstance();
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		currentCalendar.add(Calendar.DATE, -7);
		String lastWeekDate = created + format1.format(currentCalendar.getTime());

		int temp = perPage;
		double pageCount = 1;
		if (noOfRepo > perPage) {
			pageCount = (double) noOfRepo / perPage;
			pageCount = Math.ceil(pageCount);
		} else {
			temp = noOfRepo;
		}

		List<Items> items = new ArrayList<>();
		try {
			for (int i = 0; i < pageCount; i++) {
				if (temp < noOfRepo && i == pageCount - 1) {
					temp = noOfRepo - (i * temp);
				}
				UriComponentsBuilder repositorySearchUrl = UriComponentsBuilder.fromHttpUrl(repostoriesSearchBaseUrl)
						.queryParam(Constants.API_PARAM_QUALIFIER, lastWeekDate)
						.queryParam(Constants.API_PARAM_SORT, sort).queryParam(Constants.API_PARAM_ORDER, order)
						.queryParam(Constants.API_PARAM_PER_PAGE, temp).queryParam(Constants.API_PARAM_PAGE, i);

				log.info("The external request api url: {}", repositorySearchUrl.build().toUri());
				ResponseEntity<Repositories> repo = restTemplate.getForEntity(repositorySearchUrl.build().toUri(),
						Repositories.class);
				int len = repo.getBody().getItems().length;
				if (len > 0) {
					for (int j = 0; j < len; j++) {
						Items item = repo.getBody().getItems()[j];
						items.add(item);
					}
				}
			}

			log.info("The response from external API is success with length:{}", items.size());
			return items;
		} catch (Exception ex) {
			log.error("Exception occured:{}", ex);
			return null;
		}
	}
}
