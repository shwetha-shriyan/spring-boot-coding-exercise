package com.telstra.codechallenge.exception;

import lombok.Data;

@Data
public class Error {

	private final String errorMessage;

}
