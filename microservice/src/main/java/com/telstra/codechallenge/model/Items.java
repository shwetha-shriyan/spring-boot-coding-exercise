package com.telstra.codechallenge.model;

import lombok.Data;

@Data
public class Items {

	private String html_url;
	private String name;
	private String description;
	private String language;
	private int watchers_count;

}
