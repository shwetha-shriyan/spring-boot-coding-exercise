package com.telstra.codechallenge.model;

import lombok.Data;

@Data
public class Repositories {

	private Items[] items;

}
