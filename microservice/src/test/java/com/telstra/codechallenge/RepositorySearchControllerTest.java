package com.telstra.codechallenge;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;

import com.telstra.codechallenge.controller.RepositorySearchController;
import com.telstra.codechallenge.exception.InvalidInputException;
import com.telstra.codechallenge.model.Items;
import com.telstra.codechallenge.service.RepositorySearchService;
import com.telstra.codechallenge.util.Constants;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class RepositorySearchControllerTest {

	@LocalServerPort
	private int port;

	@Mock
	RepositorySearchService repositorySearchService;

	@InjectMocks
	RepositorySearchController repositorySearchController;

	@Test
	public void getRepositoriesSuccess() {
		List<Items> itemsList = new ArrayList<>();
		Items item = new Items();
		item.setHtml_url("https://github.com/karpathy/minGPT");
		item.setName("minGPT");
		item.setDescription(
				"A minimal PyTorch re-implementation of the OpenAI GPT (Generative Pretrained Transformer) training");
		item.setLanguage("Jupyter Notebook");
		item.setWatchers_count(1670);
		itemsList.add(item);

		Mockito.when(repositorySearchService.getRepositories(1)).thenReturn(itemsList);
		assertEquals(itemsList, repositorySearchController.getRepositories(1).getBody());
	}

	@Test
	public void getRepositoriesFailure() {

		assertThrows(InvalidInputException.class, () -> repositorySearchController.getRepositories(-1),
				Constants.INVALID_INPUT);
	}

}
