package com.telstra.codechallenge;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import com.telstra.codechallenge.model.Items;
import com.telstra.codechallenge.model.Repositories;
import com.telstra.codechallenge.service.RepositorySearchService;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)

public class RepositorySearchServiceTest {

	@Value("${api.github.base.url}")
	private String repostoriesSearchBaseUrl;

	@Value("${api.github.param.qualifier.value}")
	private String created;

	@Value("${api.github.param.sort.value}")
	private String sort;

	@Value("${api.github.param.order.value}")
	private String order;

	@Value("${page.per.size}")
	private int perPage;

	@Mock
	private RestTemplate restTemplate;

	@InjectMocks
	RepositorySearchService repositorySearchService;

	@BeforeEach
	public void setUp() throws Exception {

		ReflectionTestUtils.setField(repositorySearchService, "repostoriesSearchBaseUrl", repostoriesSearchBaseUrl);
		ReflectionTestUtils.setField(repositorySearchService, "created", created);
		ReflectionTestUtils.setField(repositorySearchService, "sort", sort);
		ReflectionTestUtils.setField(repositorySearchService, "order", order);
		ReflectionTestUtils.setField(repositorySearchService, "perPage", perPage);
	}

	@Test
	public void getRepositoriesForDefaultValue() {
		List<Items> itemsList = new ArrayList<>();
		Items[] newItems = new Items[1];
		Items item = new Items();
		item.setHtml_url("https://github.com/karpathy/minGPT");
		item.setName("minGPT");
		item.setDescription(
				"A minimal PyTorch re-implementation of the OpenAI GPT (Generative Pretrained Transformer) training");
		item.setLanguage("Jupyter Notebook");
		item.setWatchers_count(1670);
		newItems[0] = item;
		itemsList.add(item);
		Repositories repo = new Repositories();
		repo.setItems(newItems);
		ResponseEntity<Repositories> response = ResponseEntity.ok(repo);

		Mockito.when(restTemplate.getForEntity(Mockito.any(), Mockito.eq(Repositories.class))).thenReturn(response);

		assertEquals(itemsList, repositorySearchService.getRepositories(5));
	}

	@Test
	public void getRepositoriesForGreaterThanDefaultValue() {
		List<Items> itemsList = new ArrayList<>();
		Items[] newItems = new Items[1];
		Items item = new Items();
		item.setHtml_url("https://github.com/karpathy/minGPT");
		item.setName("minGPT");
		item.setDescription(
				"A minimal PyTorch re-implementation of the OpenAI GPT (Generative Pretrained Transformer) training");
		item.setLanguage("Jupyter Notebook");
		item.setWatchers_count(1670);
		newItems[0] = item;
		itemsList.add(item);
		itemsList.add(item);

		Repositories repo = new Repositories();
		repo.setItems(newItems);
		ResponseEntity<Repositories> response = ResponseEntity.ok(repo);

		Mockito.when(restTemplate.getForEntity(Mockito.any(), Mockito.eq(Repositories.class))).thenReturn(response);

		assertEquals(itemsList, repositorySearchService.getRepositories(100));
	}

}
